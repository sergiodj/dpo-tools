#!/bin/sh
#
#    Copyright (C) 2008 Adriano Rafael Gomes <adrianorg@arg.eti.br>
#    Copyright (C) 2018 Debian Brazilian Localization Team <contato@debianbrasil.org.br>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [ -z "$1" ]
then
  echo "po file is missing\n"
  exit 1
fi

po=$1
pkg=${po%_pt_BR*}
templates=$(
  ls -1 $pkg*_templates.pot 2> /dev/null |
  sort --numeric-sort |
  tail --lines=1
) # nome do arquivo de templates mais recente

if [ -z "$templates" ]
then
  echo "There is no file with templates to patch\n"
  exit 1
fi

rm $po~ 2> /dev/null
echo "Patching $templates\n"
msgmerge -U $po $templates

if [ -f $po~ ]
then
  diff -u $po~ $po | dpo-diff
else
  echo "There was no change when patching templates\n"
fi
